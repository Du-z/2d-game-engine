#ifndef H_GAME
#define H_GAME

#include "SFML\Graphics.hpp"
#include "SFML\Audio\Music.hpp"
#include "app\screen.h"

class App
{
public:
	~App();
	static App* GetInstance();

	void setup();
	void loop();
	void destroy();

private:
	App();
	static App* mInstance;

	sf::RenderWindow* window;
	ABScreen* theScreen;

	ActorMgr actorMgr;

	sf::Music music;
	std::string curMusic;
};

#endif // H_GAME