#include "app.h"

#include "kf\kf_log.h"
#include "qgf2d\system.h"

#include "app\screens\mainMenu.h"
#include "app\screens\gameLoop.h"
#include "app\screens\credits.h"
#include "app\screens\pause.h"

#include "app\resourceMgr.h"
#include "app\eventMgr.h"
#include "app\soundMgr.h"

App* App::mInstance = NULL;

App* App::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new App();

	return mInstance;
}

App::App()
{
	window = NULL;
	theScreen = NULL;
}

App::~App()
{
	window = NULL;
	theScreen = NULL;

    mInstance = NULL;
}

void App::setup()
{
	qgf::initDirectory();
	kf::Log::getDefault().addCout();
	kf::Log::getDefault().addFile("base.log");
	kf_log(appName + " Starting");

	window = new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight, 32), appName);
	window->setFramerateLimit(60);

	ResourceMgr::GetInstance();
	EventMgr::GetInstance();
	SoundMgr::GetInstance();

	theScreen = new MainMenu();
	theScreen->setup(*window);

	curMusic = "data/music/menu.ogg";
	if (!music.openFromFile(curMusic))
	{
		kf_log("Could not load '" + curMusic + "'");
	}
	else
	{
		kf_log("Loaded '" + curMusic + "'");
	}
	music.setLoop(true);
	music.play();

	kf_log("Game Init Finished");
}

void App::loop()
{
	sf::Clock deltaClock;
	float deltaT;

	while (window->isOpen())
	{
		deltaT = deltaClock.restart().asSeconds();

		EventMgr::GetInstance()->notifySubscribers(*theScreen);
		theScreen->simulation(*window, deltaT);
		theScreen->render(*window);

		//Next screen check
		ScreenTypes nextScreen = theScreen->screenChange();
		if(nextScreen != NO_CHANGE)
		{
			kf_log("Switching Screens");
			theScreen->dispose();
			EventMgr::GetInstance()->clearSubscribers();
			delete theScreen;

			switch (nextScreen)
			{
			case MAIN_MENU:
				theScreen = new MainMenu();
				break;
			case GAME_LOOP:
				theScreen = new GameLoop();
				break;
			case CREDITS:
				theScreen = new Credits();
				break;
			case EXIT:
				window->close();
				theScreen = NULL;
				break;
			default:
				window->close();
				theScreen = NULL;
				break;
			}

			if(theScreen != NULL)
			{
				theScreen->setup(*window);

				sf::View view = window->getView();
				if(nextScreen == GAME_LOOP)
				{
					view.reset(sf::FloatRect(0, 0, screenWidth * 2, screenHeight * 2));

					if(curMusic != "data/music/gamePlay.ogg")
					{
						curMusic = "data/music/gamePlay.ogg";
						if (!music.openFromFile(curMusic))
						{
							kf_log("Could not load '" + curMusic + "'");
						}
						else
						{
							kf_log("Loaded '" + curMusic + "'");
						}
						music.play();
					}
				}
				else
				{
					view.reset(sf::FloatRect(0, 0, screenWidth, screenHeight));

					if(curMusic != "data/music/menu.ogg")
					{
						curMusic = "data/music/menu.ogg";
						if (!music.openFromFile(curMusic))
						{
							kf_log("Could not load '" + curMusic + "'");
						}
						else
						{
							kf_log("Loaded '" + curMusic + "'");
						}
						music.play();
					}
				}
				window->setView(view);
			}
		}

		//std::cout << 1 / deltaT << '\n';
		SoundMgr::GetInstance()->update();
	}
}

void App::destroy()
{
	delete window;
	window = NULL;

	delete theScreen;
	theScreen = NULL;

	ResourceMgr::GetInstance()->destroyAll();
	SoundMgr::GetInstance()->dispose();
}
