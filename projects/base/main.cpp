#include "app.h"

int main()
{	
	App* pGame = App::GetInstance();

	pGame->setup();
	pGame->loop();
	pGame->destroy();

	delete pGame;
	pGame = NULL;

	return 0;
}
