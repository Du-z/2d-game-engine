#ifndef H_UI_MGR_ACTOR
#define H_UI_MGR_ACTOR

#include "app\actorMgr\actor.h"
#include "app\actorMgr\actor\uiMgr\uiElement.h"

#include "app\actorMgr\actor\uiMgr\uiElement\label.h"
#include "app\actorMgr\actor\uiMgr\uiElement\ninePatch.h"

#include "app\actorMgr\actor\uiMgr\uiElement\Button.h"

class ABUiElement;

class UiMgr : public ABActor
{
public:
	UiMgr();
	virtual ~UiMgr();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:

	std::vector<ABUiElement*> uiList;

};

#endif // H_UI_MGR_ACTOR