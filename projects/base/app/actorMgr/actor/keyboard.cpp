#include "app\actorMgr\actor\keyboard.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\keyPressed.h"

Keyboard::Keyboard()
{

	mActorType = Actor::KEYBOARD;
}

Keyboard::~Keyboard()
{

}

void Keyboard::setup(b2World* pB2World, const Json::Value& properties)
{

}

void Keyboard::simulation(sf::RenderWindow& window, float deltaT)
{
	// Loop through every key to see if they have been pressed.
	sf::Keyboard::Key key = sf::Keyboard::Unknown;
	for(int i = 0; i < sf::Keyboard::KeyCount ; ++i)
	{
		key = static_cast<sf::Keyboard::Key>(i);
		if(sf::Keyboard::isKeyPressed(key))
			EventMgr::GetInstance()->addEvent(new KeyPressed(key));
	}
}

void Keyboard::eventReceiver(ABEvent* theEvent)
{

}

void Keyboard::render(sf::RenderWindow& window)
{

}

void Keyboard::dispose()
{

}
