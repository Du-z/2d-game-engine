#include "app\actorMgr\actor\particleMgr.h"

#include "app\eventMgr.h"
#include "app\resourceMgr.h"

#include "app\eventMgr\event\mousePressed.h"

ParticleMgr::ParticleMgr()
{
	mActorType = Actor::PARTICLE_MGR;
}

ParticleMgr::~ParticleMgr()
{

}

void ParticleMgr::setup(b2World* pB2World, const Json::Value& properties)
{
	Json::Value& particles = *ResourceMgr::GetInstance()->getJson("data/config/particleTypes.json");

	// quickly calculate how many emitters will be needed
	totalEmitters = 0;
	for(size_t i = 0; i < particles["particles"].size(); ++i)
	{
		totalEmitters += particles["particles"][i]["emitterCount"].asInt();
	}
	
	emitterList.resize(totalEmitters);
	
	for(size_t i = 0, k = 0; i < particles["particles"].size(); ++i)
	{
		size_t numEmitters = particles["particles"][i]["emitterCount"].asInt();
		for(size_t j = 0; j < numEmitters; ++j, ++k)
		{
			emitterList[k].setup(particles["particles"][i]);
		}
	}

	// we don't need the json anymore
	ResourceMgr::GetInstance()->destroyJson("data/config/particleTypes.json");

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::MOUSE_PRESSED, this);
	//////////////////////////////////////////////
}

void ParticleMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].simulation(deltaT);
	}
}

void ParticleMgr::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::MOUSE_PRESSED)
	{
		MousePressed* button = static_cast<MousePressed*>(theEvent);
	
		if(button->getButton() == sf::Mouse::Left)
		{
			sf::Vector2f pos(button->getPos());
			float angleDirection = 1.57079633f;

			for(size_t i = 0; i < totalEmitters; ++i)
			{
				emitterList[i].start(pos, angleDirection);
			}
		}
	}
}

void ParticleMgr::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].render(window);
	}
}

void ParticleMgr::dispose()
{
	for(size_t i = 0; i < totalEmitters; ++i)
	{
		emitterList[i].dispose();
	}

	emitterList.clear();
}
