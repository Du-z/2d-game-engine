#include "app\actorMgr\actor\ground.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\keyPressed.h"

Ground::Ground()
{
	mActorType = Actor::GROUND;
}

Ground::~Ground()
{

}

void Ground::setup(b2World* pB2World, const Json::Value& properties)
{

	/////////////// SETUP GENERAL ////////////////

	//////////////////////////////////////////////

	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(screenWidthM / 2, screenHeightM - screenHeightM / 4);
	bodyDef.type = b2_staticBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2Vec2 size(10, 1); // Set size here
	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f((float) pDrawable->getSprite()->getTextureRect().width / 2, (float) pDrawable->getSprite()->getTextureRect().height / 2));
	pDrawable->setColour(sf::Color::Blue);
	pDrawable->setPosRot(*pBody);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////

	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	
	//////////////////////////////////////////////
}

void Ground::simulation(sf::RenderWindow& window, float deltaT)
{
	
}

void Ground::eventReceiver(ABEvent* theEvent)
{
	
}

void Ground::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Ground::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}
