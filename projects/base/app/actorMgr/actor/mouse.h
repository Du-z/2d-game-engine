#ifndef H_MOUSE_ACTOR
#define H_MOUSE_ACTOR

#include "app\actorMgr\actor.h"

class Mouse : public ABActor
{
public:
	Mouse();
	virtual ~Mouse();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_MOUSE_ACTOR