#ifndef H_LABEL_UI
#define H_LABEL_UI

#include "sfml\Graphics\Text.hpp"

#include "app\actorMgr\actor\uiMgr\uiElement.h"

class Label : public ABUiElement
{
public:
	Label();
	virtual ~Label();

	virtual void setup(const Json::Value& properties);
	virtual void simulation(float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	sf::Text mText;
};

#endif // H_LABEL_UI