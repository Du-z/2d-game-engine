#ifndef H_NINE_PATCH_UI
#define H_NINE_PATCH_UI

#include "app\drawable.h"
#include "app\actorMgr\actor\uiMgr\uiElement.h"

class NinePatch : public ABUiElement
{
public:
	NinePatch();
	virtual ~NinePatch();

	virtual void setup(const Json::Value& properties);
	virtual void simulation(float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void setPos(const sf::Vector2i& p);
	void setSize(const sf::Vector2i& s);
	void setColour(const sf::Color& col);

	const sf::Vector2i& getPos(){return pos;}
	const sf::Vector2i& getSize(){return size;}
	const sf::Color& getColour(){return colour;}

private:
	int left, top, right, bottom;
	sf::Vector2i size;
	sf::Vector2i pos;
	sf::Color colour;

	std::vector<Drawable*> spriteList;
};

#endif // H_NINE_PATCH_UI