#ifndef H_BUTTON_UI
#define H_BUTTON_UI

#include "app\actorMgr\actor\uiMgr\uiElement.h"

#include "app\actorMgr\actor\uiMgr\uiElement\label.h"
#include "app\actorMgr\actor\uiMgr\uiElement\ninePatch.h"

class Button : public ABUiElement
{
public:
	Button();
	virtual ~Button();

	virtual void setup(const Json::Value& properties);
	virtual void simulation(float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	NinePatch mNinePatch;
	Label mLabel;

	bool isHovering, isPressed, wasPressed;
};

#endif // H_BUTTON_UI