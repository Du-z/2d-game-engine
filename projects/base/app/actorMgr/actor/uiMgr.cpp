#include "app\actorMgr\actor\uiMgr.h"

#include "kf\kf_log.h"

#include "app\eventMgr.h"
#include "app\resourceMgr.h"

UiMgr::UiMgr()
{
	mActorType = Actor::UI_MGR;
}

UiMgr::~UiMgr()
{

}

void UiMgr::setup(b2World* pB2World, const Json::Value& properties)
{
	size_t numUiElements = properties["elements"].size();

	std::hash<std::string> hashFn;
	size_t uiNameHash;

	for(size_t i = 0, size = numUiElements; i < size; ++i)
	{
		const Json::Value& element = properties["elements"][i];
		ABUiElement* uiElement = NULL;
		uiNameHash = hashFn(element["type"].asString());

		if(uiNameHash == hashFn("label"))
			uiElement = new Label();
		if(uiNameHash == hashFn("ninePatch"))
			uiElement = new NinePatch();
		if(uiNameHash == hashFn("button"))
			uiElement = new Button();

		if(uiElement != NULL)
		{
			uiElement->setup(element);
			uiList.push_back(uiElement);
		}
		else
		{
			kf_log("Could not load UI element '" + element["name"].asString() + "' - Not Defined");
		}
	}

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::MOUSE_PRESSED, this);
	EventMgr::GetInstance()->addSubscriber(Event::MOUSE_POSITION, this);
	//////////////////////////////////////////////
}

void UiMgr::simulation(sf::RenderWindow& window, float deltaT)
{
	for(size_t i = 0; i < uiList.size(); ++i)
	{
		uiList[i]->simulation(deltaT);
	}
}

void UiMgr::eventReceiver(ABEvent* theEvent)
{
	for(size_t i = 0; i < uiList.size(); ++i)
	{
		uiList[i]->eventReceiver(theEvent);
	}
}

void UiMgr::render(sf::RenderWindow& window)
{
	for(size_t i = 0; i < uiList.size(); ++i)
	{
		uiList[i]->render(window);
	}
}

void UiMgr::dispose()
{
	for(size_t i = 0; i < uiList.size(); ++i)
	{
		uiList[i]->dispose();
		delete uiList[i];
	}

	uiList.clear();
}
