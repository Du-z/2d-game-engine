#ifndef H_PLAYER_ACTOR
#define H_PLAYER_ACTOR

#include "app\actorMgr\actor.h"

class Player : public ABActor
{
public:
	Player();
	virtual ~Player();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	Drawable* pDrawable;
	b2Body* pBody;

	b2Vec2 xPower, yPower;

};

#endif // H_PLAYER_ACTOR