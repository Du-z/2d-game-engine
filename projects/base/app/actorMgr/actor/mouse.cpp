#include "app\actorMgr\actor\mouse.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\mousePressed.h"
#include "app\eventMgr\event\mousePos.h"

Mouse::Mouse()
{

	mActorType = Actor::MOUSE;
}

Mouse::~Mouse()
{

}

void Mouse::setup(b2World* pB2World, const Json::Value& properties)
{

}

void Mouse::simulation(sf::RenderWindow& window, float deltaT)
{
	// Loop through every mouse button to see if they have been pressed.
	sf::Mouse::Button button = sf::Mouse::ButtonCount;
	for(int i = 0; i < sf::Mouse::ButtonCount ; ++i)
	{
		button = static_cast<sf::Mouse::Button>(i);
		if(sf::Mouse::isButtonPressed(button))
			EventMgr::GetInstance()->addEvent(new MousePressed(button, sf::Mouse::getPosition(window)));
	}

	EventMgr::GetInstance()->addEvent(new MousePos(sf::Mouse::getPosition(window)));
}

void Mouse::eventReceiver(ABEvent* theEvent)
{

}

void Mouse::render(sf::RenderWindow& window)
{

}

void Mouse::dispose()
{

}
