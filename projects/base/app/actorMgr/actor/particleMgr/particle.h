#ifndef H_PARTICLE
#define H_PARTICLE

#include "SFML\System\Vector2.hpp"

#include "app\drawable.h"

class Particle
{
public:
	Particle();
	~Particle();

	void setup(Drawable& drawable);
	void start(const sf::Vector2f& pos, float speed, float radian, float spin, sf::Color colour);

	inline void simulation(float deltaT)
	{
		mPos += mVel * deltaT;
		mRotation += mSpin * deltaT;
	}

	inline void render(sf::RenderWindow& window)
	{
		pDrawable->setPosRot(mPos, mRotation);
		pDrawable->setColour(mColour);
		pDrawable->draw(window);
	}

private:
	Drawable* pDrawable; // hold the pointer to the particle emitter drawable

	sf::Vector2f mPos; // current Pos
	float mRotation; // current angle
	sf::Color mColour; // current colour

	sf::Vector2f mVel; // Velocity/s
	float mSpin; // radian/s
};

#endif // H_PARTICLE