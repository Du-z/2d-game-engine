#define _USE_MATH_DEFINES // to enable non-std math const

#include "app\actorMgr\actor\particleMgr\particle.h"

#include "general.h"

Particle::Particle()
{
}

Particle::~Particle()
{
}

void Particle::setup(Drawable& drawable)
{
	pDrawable = &drawable;
}

void Particle::start( const sf::Vector2f& pos, float speed, float radian, float spin, sf::Color colour)
{
	mVel = sf::Vector2f(speed * cos(radian), speed * sin(radian));
	mPos = pos;
	mSpin = spin;
	mRotation = radian * 180 / (float)M_PI - 90;
	mColour = colour;
}