#ifndef H_PARTICLE_MGR_ACTOR
#define H_PARTICLE_MGR_ACTOR

#include "app\actorMgr\actor.h"

#include "app\actorMgr\actor\particleMgr\particleEmitter.h"

class ParticleMgr : public ABActor
{
public:
	ParticleMgr();
	virtual ~ParticleMgr();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	size_t totalEmitters;

	std::vector<ParticleEmitter> emitterList;

};

#endif // H_PARTICLE_MGR_ACTOR