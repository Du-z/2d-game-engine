#ifndef H_GROUND_ACTOR
#define H_GROUND_ACTOR

#include "app\actorMgr\actor.h"

class Ground : public ABActor
{
public:
	Ground();
	virtual ~Ground();

	virtual void setup(b2World* pB2World, const Json::Value& properties);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	Drawable* pDrawable;
	b2Body* pBody;
};

#endif // H_GROUND_ACTOR