#include "app\actorMgr\actor\player.h"

#include "app\eventMgr.h"
#include "app\eventMgr\event\keyPressed.h"

Player::Player()
{
	mActorType = Actor::PLAYER;
}

Player::~Player()
{

}

void Player::setup(b2World* pB2World, const Json::Value& properties)
{

	/////////////// SETUP GENERAL ////////////////
	xPower.Set(0.1f, 0);
	yPower.Set(0, 0.1f);
	//////////////////////////////////////////////

	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(screenWidthM / 2, screenHeightM / 2);
	bodyDef.type = b2_dynamicBody;
	pBody = pB2World->CreateBody(&bodyDef);

	b2Vec2 size(1, 1); // Set size here
	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.density = 0.1f;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f((float) pDrawable->getSprite()->getTextureRect().width / 2, (float) pDrawable->getSprite()->getTextureRect().height / 2));
	pDrawable->setColour(sf::Color::Red);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////

	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::KEY_PRESSED, this);
	//////////////////////////////////////////////
}

void Player::simulation(sf::RenderWindow& window, float deltaT)
{
	pDrawable->setPosRot(*pBody);
}

void Player::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::KEY_PRESSED)
	{
		KeyPressed* key = static_cast<KeyPressed*>(theEvent);

		switch (key->getKey())
		{
			case sf::Keyboard::Right:
				pBody->ApplyLinearImpulse(xPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::Left:
				pBody->ApplyLinearImpulse(-xPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::Up:
				pBody->ApplyLinearImpulse(-yPower, pBody->GetWorldCenter());
				break;
			case sf::Keyboard::Down:
				pBody->ApplyLinearImpulse(yPower, pBody->GetWorldCenter());
				break;
		}
	}
}

void Player::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Player::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}
