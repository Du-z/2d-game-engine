#ifndef H_ACTOR_ENUM
#define H_ACTOR_ENUM

// the game loop will sort the actor list into the same order that these enums are in.
namespace Actor
{
	enum Type
	{
		UNSET = 0, 
		KEYBOARD,
		MOUSE,
		BACKGROUND,
		GROUND,
		PLAYER,
		PARTICLE_MGR,
		UI_MGR,
		COUNT
	};
}

#endif // H_ACTOR