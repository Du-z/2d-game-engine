#include "app\soundMgr.h"
#include "kf\kf_log.h"

SoundMgr* SoundMgr::mInstance = NULL;

SoundMgr* SoundMgr::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new SoundMgr();

	return mInstance;
}

SoundMgr::SoundMgr()
{
	mId = 0;

	masterVol = 1;
	sfxVol = 1;
}

SoundMgr::~SoundMgr()
{
	mInstance = NULL;
}

int SoundMgr::play(const std::string& dir, const sf::Vector2f& pos)
{
	sf::SoundBuffer* buffer = ResourceMgr::GetInstance()->getSound(dir);
	if(buffer == NULL)
		return -1;

	sf::Sound *sound = new sf::Sound(*buffer);

	float vol = 100 * sfxVol * masterVol;
	sound->setVolume(vol);
	sound->play();

	mSounds[++mId] = sound;
	return mId;
}

void SoundMgr::pause(int id)
{
	if(isAvailable(id))
	{
		mSounds[id]->pause();
	}
}

void SoundMgr::unpause(int id)
{
	if(isAvailable(id))
	{
		mSounds[id]->play();
	}
}

void SoundMgr::stop(int id)
{
	if(isAvailable(id))
	{
		mSounds[id]->stop();
	}
}

void SoundMgr::pauseAll()
{
	for(std::map<int, sf::Sound*>::iterator it = mSounds.begin(); it != mSounds.end(); ++it)
	{
		it->second->pause();
	}
}

void SoundMgr::unpauseAll()
{
	for(std::map<int, sf::Sound*>::iterator it = mSounds.begin(); it != mSounds.end(); ++it)
	{
		it->second->play();
	}
}

void SoundMgr::stopAll()
{
	for(std::map<int, sf::Sound*>::iterator it = mSounds.begin(); it != mSounds.end(); ++it)
	{
		it->second->stop();
	}
}

bool SoundMgr::isAvailable(int id)
{
	if(mSounds.find(id) != mSounds.end())
		return true;

	return false;
}

bool SoundMgr::isPlaying(int id)
{
	if(isAvailable(id) && mSounds[id]->getStatus() == sf::Sound::Status::Playing)
		return true;
	return false;
}

bool SoundMgr::isPaused(int id)
{
	if(isAvailable(id) && mSounds[id]->getStatus() == sf::Sound::Status::Paused)
		return true;
	return false;
}

bool SoundMgr::isStopped(int id)
{
	if(isAvailable(id) && mSounds[id]->getStatus() == sf::Sound::Status::Stopped)
		return true;
	return false;
}

sf::Sound::Status SoundMgr::getStatus(int id)
{
	if(isAvailable(id))
		return mSounds[id]->getStatus();
	return sf::Sound::Status::Stopped;
}

void SoundMgr::setVol(int id, float vol)
{
	if(isAvailable(id))
	{
		if(vol > 1)
			vol = 1;
		else if (vol < 0)
			vol = 0;
		
		float v = 100 * vol * sfxVol * masterVol;
		mSounds[id]->setVolume(v);
	}
}

void SoundMgr::setPitch(int id, float pitch)
{
	if(isAvailable(id))
		mSounds[id]->setPitch(pitch);
}

void SoundMgr::setPos(int id, float x, float y)
{
	if(isAvailable(id))
		mSounds[id]->setPosition(x, y, 0);
}

void SoundMgr::setLoop(int id, bool b)
{
	if(isAvailable(id))
		mSounds[id]->setLoop(b);
}

void SoundMgr::update()
{
	for(std::map<int, sf::Sound*>::iterator it = mSounds.begin(); it != mSounds.end();)
	{
		if(it->second->getStatus() == sf::Sound::Status::Stopped)
		{
			delete it->second;
			it = mSounds.erase(it);
		}
		else
		{
			++it;
		}
	}
}

void SoundMgr::dispose()
{
	for(std::map<int, sf::Sound*>::iterator it = mSounds.begin(); it != mSounds.end(); ++it)
	{
		if(it->second != NULL)
		{
			delete it->second;
		}
	}
	mSounds.clear();
}