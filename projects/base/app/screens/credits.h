#ifndef H_CREDITS
#define H_CREDITS

#include "app\screen.h"

class Credits : public ABScreen
{
public:
	Credits();
	virtual ~Credits();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_CREDITS