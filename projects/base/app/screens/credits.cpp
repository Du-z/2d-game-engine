#include "app\screens\credits.h"

#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"
#include "kf\kf_vector2.h"

#include "app\resourceMgr.h"
#include "app\soundMgr.h"

Credits::Credits()
{

}

Credits::~Credits()
{

}

void Credits::setup(sf::RenderWindow& window)
{	
	//*** General Setup **//
	std::string screenName = "credits";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);

	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], NULL);

	// sort the list based on enum order
	actorMgr.sortActors();
}

void Credits::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
		else if(ev.type == sf::Event::KeyPressed)
		{
			if(ev.key.code == sf::Keyboard::Space)
			{
				nextScreen = MAIN_MENU;
				SoundMgr::GetInstance()->play("data/sounds/ui/click.ogg");
			}
		}
	}

	// simulate the vector of actors
	actorMgr.simulation(window, deltaT);
}

void Credits::eventReceiver(ABEvent* theEvent)
{

}

void Credits::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	actorMgr.render(window);

	window.display();
}

void Credits::dispose()
{
	actorMgr.dispose();
}