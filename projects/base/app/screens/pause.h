#ifndef H_PAUSE
#define H_PAUSE

#include "app\screen.h"

class Pause : public ABScreen
{
public:
	Pause();
	virtual ~Pause();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void makePaused(){paused = true;}
	bool isPaused(){return paused;}

private:
	bool paused;
};

#endif // H_PAUSE