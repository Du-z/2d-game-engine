#include "app\screens\gameLoop.h"

#include <sstream>
#include "iostream"
#include "kf\kf_log.h"
#include "SFML\Window.hpp"

#include "general.h"
#include "app\resourceMgr.h"
#include "app\screens\gameLoop\debugDraw.h"

GameLoop::GameLoop()
{
	collisionMgr = NULL;

	pauseScreen = NULL;
}

GameLoop::~GameLoop()
{

}

void GameLoop::setup(sf::RenderWindow& window)
{	
	//*** General Setup **//
	std::string screenName = "gameLoop";

	std::hash<std::string> hashFn;
	nameHash = hashFn(screenName);
	
	//*** Setup Physics **//
	b2Vec2 gravity(0, 0); //no gravity

	pB2World = new b2World(gravity);
	pB2World->SetAllowSleeping(true);

	collisionMgr = new CollisionMgr();
	pB2World->SetContactListener(collisionMgr);

	// I need to make a preprocessor for this so it wont run in release
	DebugDraw* debugdraw = new DebugDraw(); 
	debugdraw->LinkTarget(window);
	pB2World->SetDebugDraw(debugdraw); // Add the debugDraw to the B2D World

	//*** ADD GAME OBJECTS **//

	Json::Value& screens = *ResourceMgr::GetInstance()->getJson("data/config/screens.json");
	Json::Value& screenConfig = screens[screenName];

	actorMgr.addActors(screenConfig["actors"], pB2World);

	// sort the list based on enum order
	actorMgr.sortActors();

	//*** SETUP PAUSE SCREEN **//
	pauseScreen = new Pause;
	pauseScreen->setup(window);
}

void GameLoop::simulation(sf::RenderWindow& window, float deltaT)
{
	if(!pauseScreen->isPaused())
	{
		sf::Event ev;
		while (window.pollEvent(ev))
		{
			if ((ev.type == sf::Event::Closed))
				nextScreen = EXIT;
			else if(ev.type == sf::Event::KeyPressed && ev.key.code == sf::Keyboard::Escape)
				pauseScreen->makePaused();
		}

		// simulate the vector of actors
		actorMgr.simulation(window, deltaT);

		pB2World->Step(1/60.0, 8, 3);

		/*
		if()
		{
			endGame();
		}
		*/
	}
	else
	{
		pauseScreen->simulation(window, deltaT);
		nextScreen = pauseScreen->screenChange();
	}
}

void GameLoop::eventReceiver(ABEvent* theEvent)
{
	if(!pauseScreen->isPaused())
	{
	}
	else
	{
		pauseScreen->eventReceiver(theEvent);
	}
}

void GameLoop::render(sf::RenderWindow& window)
{
	if(!pauseScreen->isPaused())
	{
		window.clear();

		// Draw the vector of actors
		actorMgr.render(window);

		//Draw the debug box2D data
		pB2World->DrawDebugData();

		window.display();}
	else
	{
		pauseScreen->render(window);
	}
}

void GameLoop::dispose()
{
	actorMgr.dispose();

	delete pB2World;
	pB2World = NULL;

	delete collisionMgr;
	collisionMgr = NULL;

	pauseScreen->dispose();
	delete pauseScreen;
	pauseScreen = NULL;
}