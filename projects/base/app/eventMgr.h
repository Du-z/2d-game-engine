#ifndef H_EVENT_MGR
#define H_EVENT_MGR

#include <vector>

#include "app\actorMgr\actor.h"
#include "app\eventMgr\event.h"
#include "app\eventMgr\eventEnum.h"
#include "app\screen.h"

class EventMgr
{
public:
	~EventMgr();
	static EventMgr* GetInstance();

	void notifySubscribers(ABScreen& screen);
	void addSubscriber(Event::Types eventType, ABActor* actor);
	void clearSubscribers();

	const std::vector<ABEvent*> getEventList(){return eventList;}

	void addEvent(ABEvent* theEvent);

private:
	EventMgr();
	static EventMgr* mInstance;

	std::vector<std::vector<ABActor*>> subscriberList;
	std::vector<ABEvent*> eventList;
};

#endif // H_EVENT_MGR