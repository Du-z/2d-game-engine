#ifndef H_SOUND_MGR
#define H_SOUND_MGR

#include <vector>

#include "SFML\Audio\Sound.hpp"

#include "app\resourceMgr.h"

class SoundMgr
{
public:
	~SoundMgr();
	static SoundMgr* GetInstance();

	// returns the ID for the sound for changing attributes later
	int play(const std::string& dir){return play(dir, sf::Vector2f(0, 0));}
	int play(const std::string& dir, const sf::Vector2f& pos);

	void pause(int id); // pause the sound, it can later be unpaused or stopped
	void unpause(int id); // plays a sound already in the map
	void stop(int id); // stops the sound and will be cleaned up later by update()

	void pauseAll(); // pause all the sounds, they can later be unpaused or stopped
	void unpauseAll(); // plays all sounds already in the map
	void stopAll(); // stops all sounds from playing
	
	bool isAvailable(int id); // returns true when the sound is still in the map

	bool isPlaying(int id); // returns true when playing
	bool isPaused(int id); // returns true when paused
	bool isStopped(int id); // returns true when stopped

	// returns the status of the sound
	// if the sound can not be found returns Stopped
	sf::Sound::Status getStatus(int id); 

	// all the set functions first check to see if the sound is still playing	
	void setVol(int id, float vol); // vol = 0-1, scales with masterVol and sfxVol
	void setPitch(int id, float pitch); // default pitch = 1;
	void setPos(int id, float x, float y); // set x, y position of sound, must have only one channel
	void setPos(int id, const sf::Vector2f& pos){setPos(id, pos.x, pos.y);}
	void setLoop(int id, bool b);

	// deletes any sounds that have stopped playing, must be called once per simulation loop
	void update();

	void dispose();

private:
	SoundMgr();
	static SoundMgr* mInstance;

	float masterVol;
	float sfxVol;

	int mId;
	std::map<int, sf::Sound*> mSounds;
};

#endif // H_SOUND_MGR