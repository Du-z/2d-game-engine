#ifndef H_ACTOR_MGR
#define H_ACTOR_MGR

#include <vector>
#include "json/value.h"

#include "app\actorMgr\actor.h"
#include "app\actorMgr\actorEnum.h"

#include "app\actorMgr\actor\keyboard.h"
#include "app\actorMgr\actor\mouse.h"
#include "app\actorMgr\actor\background.h"
#include "app\actorMgr\actor\particleMgr.h"
#include "app\actorMgr\actor\player.h"
#include "app\actorMgr\actor\uiMgr.h"

class ActorMgr
{
public:
	ActorMgr();
	~ActorMgr();

	// add a list of actors to the actorList from a json file
	void addActors(const Json::Value& screenConfig, b2World* pB2World); 
	void simulation(sf::RenderWindow& window, float deltaT);
	void render(sf::RenderWindow& window);
	void dispose();

	void sortActors();

private:

	std::vector<ABActor*> actorList;
};

#endif // H_ACTOR_MGR