#ifndef H_COLLISION_MGR
#define H_COLLISION_MGR

#include "Box2D\Dynamics\b2WorldCallbacks.h"
#include "Box2D\Dynamics\Contacts\b2Contact.h"
#include "Box2D\Collision\b2Collision.h"

// this is almost run as a secondary event manager, that focuses just on collisions.
// it calls Actor::eventReceiver() like the event manager, but does not add itself to the EventMgr::eventList(), it notifies the actor directly.
class CollisionMgr : public b2ContactListener
{
public:
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
	void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);

};

#endif // H_COLLISION_MGR