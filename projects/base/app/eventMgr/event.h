#ifndef H_EVENT
#define H_EVENT

#include "app\eventMgr\eventEnum.h"

class ABEvent
{
public:
	ABEvent();
	virtual ~ABEvent();

	virtual Event::Types getEventType(){return mEventType;}

protected:
	Event::Types mEventType;

};

#endif // H_EVENT