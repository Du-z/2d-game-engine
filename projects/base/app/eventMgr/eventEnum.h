#ifndef H_EVENT_ENUM
#define H_EVENT_ENUM

namespace Event
{
	enum Types 
	{
		UNSET = -1,
		KEY_PRESSED,
		MOUSE_PRESSED,
		MOUSE_POSITION,
		UI_ELEMENT_CLICKED,
		B2D_COLLISION,
		COUNT
	};
}

#endif // H_EVENT_ENUM