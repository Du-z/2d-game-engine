#include "app\eventMgr\event\uiElementClicked.h"

UiElementClicked::UiElementClicked(Ui::Type uiType, size_t elementName)
{
	mEventType = Event::UI_ELEMENT_CLICKED;

	mUiType = uiType;
	mElementName = elementName;
}

UiElementClicked::~UiElementClicked()
{

}