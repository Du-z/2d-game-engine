#include "app\eventMgr\event\mousePressed.h"

MousePressed::MousePressed(const sf::Mouse::Button button, const sf::Vector2i& pos)
{
	mEventType = Event::MOUSE_PRESSED;

	mButton = button;
	mPos = pos;
}

MousePressed::~MousePressed()
{

}