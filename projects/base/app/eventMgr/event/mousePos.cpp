#include "app\eventMgr\event\mousePos.h"

MousePos::MousePos(const sf::Vector2i& pos)
{
	mEventType = Event::MOUSE_POSITION;

	mPos = pos;
}

MousePos::~MousePos()
{

}