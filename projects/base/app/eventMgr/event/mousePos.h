#ifndef H_MOUSE_POS
#define H_MOUSE_POS

#include "app\eventMgr\event.h"

#include "SFML\Window\Mouse.hpp"

class MousePos : public ABEvent
{
public:
	explicit MousePos(const sf::Vector2i& pos);
	virtual ~MousePos();

	sf::Vector2i getPos(){return mPos;}

private:

	sf::Vector2i mPos;
};

#endif // H_MOUSE_POS