#ifndef H_UI_ELEMENT_CLICKED
#define H_UI_ELEMENT_CLICKED

#include "app\eventMgr\event.h"

#include "app\actorMgr\actor\uiMgr\uiEnum.h"

class UiElementClicked : public ABEvent
{
public:
	UiElementClicked(Ui::Type uiType, size_t elementName);
	virtual ~UiElementClicked();

	Ui::Type getUiType(){return mUiType;}
	size_t getElementName(){return mElementName;}

private:

	Ui::Type mUiType;
	size_t mElementName;
};

#endif // H_UI_ELEMENT_CLICKED