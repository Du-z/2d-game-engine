#ifndef H_COLLISION
#define H_COLLISION

#include "app\eventMgr\event.h"
#include "app\actorMgr\actor.h"

class Collision : public ABEvent
{
public:
	explicit Collision(ABActor* otherBody);
	virtual ~Collision();

	ABActor* getOtherBody(){return mOtherBody;}

private:

	ABActor* mOtherBody;
};

#endif // H_COLLISION