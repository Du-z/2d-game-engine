#ifndef H_RESOURCE_MGR
#define H_RESOURCE_MGR

#include <string>
#include <map>
#include <vector>

#include "SFML\Graphics.hpp"
#include "SFML\Audio\SoundBuffer.hpp"
#include "json\value.h"

class ResourceMgr
{
public:
	~ResourceMgr();
	static ResourceMgr* GetInstance();

	// Returns the pointer to the data if it exist, else uses load().
	// If the file does not exist, creates a 1x1 magenta pixel
	sf::Texture* getTexture(const std::string& dir);
	bool destroyTexture(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllTextures();

	// Returns the pointer to the data if it exist, else uses load().
	// If the file does not exist, returns NULL
	sf::Font* getFont(const std::string& dir);
	bool destroyFont(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllFonts();

	// Returns the pointer to the data if it exist, else uses load().
	// If the file does not exist, returns NULL
	sf::SoundBuffer* getSound(const std::string& dir);
	bool destroySound(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllSounds();

	// Returns the pointer to the data if it exist, else uses load().
	// If the file does not exist, returns NULL
	// If the file fails to parse, returns NULL
	Json::Value* getJson(const std::string& dir);
	bool destroyJson(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllJsons();

	// removes all the resources from the maps.
	void destroyAll();

private:
	ResourceMgr();
	static ResourceMgr* mInstance;

	// Returns pointer if that resource was successfully loaded, NULL
	// The returned vector MUST be cleaned up by the caller, load() will not do it!
	std::vector<char>* load(const std::string& dir); 

	std::map<std::string, sf::Texture*> textureList;
	std::map<std::string, sf::Font*> fontList;
	std::map<std::string, sf::SoundBuffer*> soundList;
	std::map<std::string, Json::Value*> jsonList;
};

#endif // H_RESOURCE_MGR